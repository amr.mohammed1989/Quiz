package com.math.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.math.R;
import com.math.adapters.QuestionsPagerAdapter;
import com.math.fragments.QuestionsPagerFragment1;
import com.math.fragments.QuestionsPagerFragment2;
import com.math.fragments.QuestionsPagerFragment3;
import com.math.fragments.QuestionsPagerFragment4;
import com.math.fragments.QuestionsPagerFragment5;
import com.math.model.FillData;
import com.math.model.Questions;
import com.math.utils.ToastMessage;

import java.util.ArrayList;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Bind(R.id.view_pager)
    ViewPager viewPager;
    @Bind(R.id.login_pager_active_first)
    ImageView imgActiveFirst;
    @Bind(R.id.login_pager_active_second)
    ImageView imgActiveSecond;
    @Bind(R.id.login_pager_active_third)
    ImageView imgActiveThird;
    @Bind(R.id.login_pager_active_fourth)
    ImageView imgActiveFourth;
//    @Bind(R.id.login_pager_active_fifth)
//    ImageView imgActiveFifth;

    QuestionsPagerAdapter adapter;
    Random randomGen;
    int index;


    ArrayList<Questions> questionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Quiz");

        questionsList = new ArrayList<>();

        randomGen = new Random();

        refreshList();

        adapter = new QuestionsPagerAdapter(getSupportFragmentManager(), questionsList);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectIndicator(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void refreshList() {
        FillData fillData = new FillData();
        questionsList.clear();
        while (questionsList.size() <= 3) {

            ArrayList<Questions> q = fillData.getData();

            index = randomGen.nextInt(q.size());

            if (!questionsList.contains(q.get(index)))
                questionsList.add(q.get(index));
        }
    }

    private void selectIndicator(int position) {
        switch (position) {
            case 0:
                imgActiveFirst.setBackgroundResource(R.drawable.rounded_indicator_active);
                imgActiveSecond.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveThird.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveFourth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
//                imgActiveFifth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                break;

            case 1:

                imgActiveFirst.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveSecond.setBackgroundResource(R.drawable.rounded_indicator_active);
                imgActiveThird.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveFourth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
//                imgActiveFifth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                break;

            case 2:
                imgActiveFirst.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveSecond.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveThird.setBackgroundResource(R.drawable.rounded_indicator_active);
                imgActiveFourth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
//                imgActiveFifth.setBackgroundResource(R.drawable.rounded_indicator_inactive);

                break;

            case 3:
                imgActiveFirst.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveSecond.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveThird.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveFourth.setBackgroundResource(R.drawable.rounded_indicator_active);
//                imgActiveFifth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                break;

            case 4:
                imgActiveFirst.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveSecond.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveThird.setBackgroundResource(R.drawable.rounded_indicator_inactive);
                imgActiveFourth.setBackgroundResource(R.drawable.rounded_indicator_inactive);
//                imgActiveFifth.setBackgroundResource(R.drawable.rounded_indicator_active);
                break;

        }
    }


    @OnClick(R.id.btn_submit)
    public void onSubmitClicked() {
        if (QuestionsPagerFragment1.CHECKED_RADIO_ID == -1 ||
                QuestionsPagerFragment2.CHECKED_RADIO_ID == -1 ||
                QuestionsPagerFragment3.CHECKED_RADIO_ID == -1 ||
                QuestionsPagerFragment4.CHECKED_RADIO_ID == -1 ||
                QuestionsPagerFragment5.CHECKED_RADIO_ID == -1) {

            ToastMessage.message(MainActivity.this, "Please fill the missing !");

        } else {
            int score = 0;
            boolean answer1 = QuestionsPagerFragment1.ANSWER1;
            if (answer1)
                ++score;
            boolean answer2 = QuestionsPagerFragment2.ANSWER2;
            if (answer2)
                ++score;
            boolean answer3 = QuestionsPagerFragment3.ANSWER3;
            if (answer3)
                ++score;
            boolean answer4 = QuestionsPagerFragment4.ANSWER4;
            if (answer4)
                ++score;
            boolean answer5 = QuestionsPagerFragment5.ANSWER5;
            if (answer5)
                ++score;

            Log.i(TAG, "onSubmitClicked: " + answer1);
            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.result_layout);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            final TextView textViewAnswer1 =
                    (TextView) dialog.findViewById(R.id.txt_score);
            Button btnRetry = (Button) dialog.findViewById(R.id.btn_retry);
            Button btnClose = (Button) dialog.findViewById(R.id.btn_close);

            textViewAnswer1.setText(" " + score);

            btnRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    refreshList();
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
            });

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    finish();
                }
            });
            dialog.show();
//        refreshList();
//        adapter.notifyDataSetChanged();

        }
    }

    @Override
    protected void onDestroy() {
        QuestionsPagerFragment1.ANSWER1 = false;
        QuestionsPagerFragment2.ANSWER2 = false;
        QuestionsPagerFragment3.ANSWER3 = false;
        QuestionsPagerFragment4.ANSWER4 = false;
        QuestionsPagerFragment5.ANSWER5 = false;
        super.onDestroy();
    }
}
