package com.math.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.math.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Start New Quiz");
    }

    @OnClick(R.id.btn_start)
    public void onClicked() {
        startActivity(new Intent(StartActivity.this, MainActivity.class));
    }
}
