package com.math.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.math.fragments.QuestionsPagerFragment1;
import com.math.fragments.QuestionsPagerFragment2;
import com.math.fragments.QuestionsPagerFragment3;
import com.math.fragments.QuestionsPagerFragment4;
import com.math.fragments.QuestionsPagerFragment5;
import com.math.model.Questions;

import java.util.ArrayList;

public class QuestionsPagerAdapter extends FragmentStatePagerAdapter {


    ArrayList<Questions> questionsList;

    public QuestionsPagerAdapter(FragmentManager fm, ArrayList<Questions> questionsList) {
        super(fm);

        this.questionsList = questionsList;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return QuestionsPagerFragment1.newInstance(position, questionsList.get(0));
            case 1:
                return QuestionsPagerFragment2.newInstance(position, questionsList.get(1));
            case 2:
                return QuestionsPagerFragment3.newInstance(position, questionsList.get(2));
            case 3:
                return QuestionsPagerFragment4.newInstance(position, questionsList.get(3));
            case 4:
                return QuestionsPagerFragment5.newInstance(position, questionsList.get(4));
            default:
                return QuestionsPagerFragment1.newInstance(position, questionsList.get(0));
        }

    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
