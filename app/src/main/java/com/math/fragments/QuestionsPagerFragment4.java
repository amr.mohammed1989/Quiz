package com.math.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.math.R;
import com.math.model.Questions;
import com.math.utils.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;


public class QuestionsPagerFragment4 extends Fragment {

    private static final String TAG = QuestionsPagerFragment4.class.getSimpleName();
    public static boolean ANSWER4;
    public static int CHECKED_RADIO_ID;

    @Bind(R.id.questionsPager_txt_questions)
    TextView questionsPagerFragmentTxtQuestions;

    @Bind(R.id.questionsFragment4_radioGroup)
    RadioGroup radioGroup;
    @Bind(R.id.questionsFragment4_radioButton_first)
    RadioButton radioButton1;
    @Bind(R.id.questionsFragment4_radioButton_second)
    RadioButton radioButton2;
    @Bind(R.id.questionsFragment4_radioButton_third)
    RadioButton radioButton3;
    @Bind(R.id.questionsFragment4_radioButton_fourth)
    RadioButton radioButton4;


    private int position;
    Questions question;

    public static QuestionsPagerFragment4 newInstance(int position, Questions questionsList) {
        QuestionsPagerFragment4 fragment = new QuestionsPagerFragment4();
        Bundle args = new Bundle();
        args.putInt(Constants.POSITON, position);
        args.putSerializable(Constants.QUESTION_LIST, questionsList);
        fragment.setArguments(args);
        return fragment;
    }


    public QuestionsPagerFragment4() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment4_questions_pager, container, false);
        ButterKnife.bind(this, rootView);

        if (getArguments() != null) {
            position = getArguments().getInt(Constants.POSITON);
            Log.i(TAG, "onCreateView: " + position);
            question = (Questions) getArguments().getSerializable(Constants.QUESTION_LIST);

            questionsPagerFragmentTxtQuestions.setText(question.question);

        }

        radioButton1.setText(question.choices.get(0).choice);
        radioButton2.setText(question.choices.get(1).choice);
        radioButton3.setText(question.choices.get(2).choice);
        radioButton4.setText(question.choices.get(3).choice);

        checkTrueAnswer(rootView);
        return rootView;
    }


    void checkTrueAnswer(final View rootView) {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                CHECKED_RADIO_ID = i;
                int index = radioGroup.indexOfChild(rootView.findViewById(radioGroup.getCheckedRadioButtonId()));
                ANSWER4 = question.choices.get(index).isTrue;
            }
        });
        CHECKED_RADIO_ID = radioGroup.getCheckedRadioButtonId();
    }

}
