package com.math.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amr on 12/07/16.
 */
public class Questions implements Serializable {

    public String question;
    public List<Choice> choices;
}

