package com.math.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by amr on 12/07/16.
 */
public class FillData {

    QuestionsAndChoices questionsAndChoices, tempArray;
    Random randomGen;

    public FillData() {

        questionsAndChoices = new QuestionsAndChoices();
        randomGen = new Random();

        Questions questions = new Questions();
        questions.choices = new ArrayList<>();

        questions.question = "What is the capital of Egypt ? ";
        ArrayList<Choice> choice = createChoices("Cairo", "Assuit", "Alexandria", "Mansoura", 0);
        questions.choices.addAll(choice);


        Questions questions2 = new Questions();
        questions2.choices = new ArrayList<>();

        questions2.question = "What is the name of the river that runs in Egypt ? ";
        ArrayList<Choice> choice2 = createChoices("Nile", "Degla", "Mississipi", "Danube", 0);
        questions2.choices.addAll(choice2);

        Questions questions3 = new Questions();
        questions3.choices = new ArrayList<>();

        questions3.question = "Which landmark is in Egypt ?";
        ArrayList<Choice> choice3 = createChoices("Pyramids", "Great Wall", "Eiffel tower", "Trevi fountain", 0);
        questions3.choices.addAll(choice3);


        Questions questions4 = new Questions();
        questions4.choices = new ArrayList<>();
        questions4.question = "What is the name of the sea that it north to Egypt ?";
        ArrayList<Choice> choice4 = createChoices("Mediterranean sea", "red sea", "black sea", "north sea", 0);
        questions4.choices.addAll(choice4);
//        for (int i = 0; i < 4; i++) {
//            Choice choice = getChoice(i, 3, 1);
//
//            questions3.choices.add(choice);
//
//        }

//        Questions questions5 = new Questions();
//        questions5.choices = new ArrayList<>();
//        questions5.question = "Q5 ? ";
//
//
//        for (int i = 0; i < 4; i++) {
//
//            Choice choice = getChoice(i, 5, 1);
//            questions5.choices.add(choice);
//
//        }
//
//        Questions questions6 = new Questions();
//        questions6.question = "Q6 ? ";
//        questions6.choices = new ArrayList<>();
//
//        for (int i = 0; i < 4; i++) {
//
//            Choice choice = getChoice(i, 6, 1);
//            questions6.choices.add(choice);
//
//        }
//
//        Questions questions7 = new Questions();
//        questions7.choices = new ArrayList<>();
//        questions7.question = "Q7 ? ";
//
//
//        for (int i = 0; i < 4; i++) {
//            Choice choice = getChoice(i, 7, 2);
//
//            questions7.choices.add(choice);
//
//        }
//
//
//        Questions questions8 = new Questions();
//        questions8.choices = new ArrayList<>();
//        questions8.question = "Q8 ? ";
//
//
//        for (int i = 0; i < 4; i++) {
//            Choice choice = getChoice(i, 8, 2);
//            questions8.choices.add(choice);
//
//        }


        questionsAndChoices.questions = new ArrayList<>();

        Collections.shuffle(questions.choices);
        Collections.shuffle(questions2.choices);
        Collections.shuffle(questions3.choices);
        Collections.shuffle(questions4.choices);
//        Collections.shuffle(questions5.choices);
//        Collections.shuffle(questions6.choices);
//        Collections.shuffle(questions7.choices);
//        Collections.shuffle(questions8.choices);

        questionsAndChoices.questions.add(questions);
        questionsAndChoices.questions.add(questions2);
        questionsAndChoices.questions.add(questions3);
        questionsAndChoices.questions.add(questions4);
//        questionsAndChoices.questions.add(questions5);
//        questionsAndChoices.questions.add(questions6);
//        questionsAndChoices.questions.add(questions7);
//        questionsAndChoices.questions.add(questions8);
        tempArray = questionsAndChoices;
    }

    private ArrayList<Choice> createChoices(String choice1, String choice2, String choice3, String choice4, int rightAnswerPosition) {
        ArrayList<Choice> list = new ArrayList<>();
        Choice ch1 = new Choice();
        ch1.choice= choice1;
        ch1.isTrue = true;
        list.add(ch1);
        ch1 = new Choice();
        ch1.choice = choice2;
        list.add(ch1);
        ch1 = new Choice();
        ch1.choice = choice3;
        list.add(ch1);
        ch1 = new Choice();
        ch1.choice = choice4;
        list.add(ch1);


        return list;
    }

    @NonNull
    private Choice getChoice(int i, int questionNumber, int trueAnswerPos) {
        Choice choice = new Choice();
        if (i == trueAnswerPos) {
            choice.isTrue = true;
            choice.choice = questionNumber + " True " + i;
        } else {
            choice.choice = questionNumber + " Answer " + i;
        }
        return choice;
    }

    public ArrayList<Questions> getData() {

        return questionsAndChoices.questions;
    }
}
